---
- name: Install and configure Nagios Core on RHEL 9.x
  hosts: ip-172-31-44-130.ec2.internal
  become: yes
  vars:
    nagios_version: "4.5.9"
    nagios_plugins_version: "2.3.3"
    apache_user: "apache"
    nagios_user: "nagios"
    nagios_admin_user: "nagiosadmin"
    nagios_admin_password: "your_secure_password_here"

  tasks:
    - name: Install required packages and dependencies
      dnf:
        name:
          - wget
          - httpd
          - php
          - gd
          - gd-devel
          - make
          - gcc
          - glibc
          - glibc-devel
          - gcc-c++
          - libpng-devel
          - libjpeg-devel
          - libcurl-devel
          - unzip
        state: present

    - name: Create nagios and nagiosadmin user
      user:
        name: "{{ item }}"
        state: present
      with_items:
        - "{{ nagios_user }}"
        - "{{ nagios_admin_user }}"

    - name: Download Nagios Core tarball
      get_url:
        url: "https://github.com/NagiosEnterprises/nagioscore/releases/download/nagios-4.5.9/nagios-4.5.9.tar.gz"
        dest: "/tmp/nagios-4.5.9.tar.gz"

    - name: Extract Nagios Core tarball
      unarchive:
        src: "/tmp/nagios-{{ nagios_version }}.tar.gz"
        dest: "/tmp/"
        remote_src: yes

    - name: Install Nagios Core
      shell: "./configure --with-httpd-conf=/etc/httpd/conf.d && make all && make install && make install-init && make install-commandmode && make install-config && make install-webconf"
      args:
        chdir: "/tmp/nagios-{{ nagios_version }}"

    - name: Download Nagios Plugins tarball
      get_url:
        url: "https://github.com/nagios-plugins/nagios-plugins/releases/download/release-{{ nagios_plugins_version }}/nagios-plugins-{{ nagios_plugins_version }}.tar.gz"
        dest: "/tmp/nagios-plugins-{{ nagios_plugins_version }}.tar.gz"

    - name: Extract Nagios Plugins tarball
      unarchive:
        src: "/tmp/nagios-plugins-{{ nagios_plugins_version }}.tar.gz"
        dest: "/tmp/"
        remote_src: yes

    - name: Install Nagios Plugins
      shell: "./configure && make && make install"
      args:
        chdir: "/tmp/nagios-plugins-{{ nagios_plugins_version }}"

    - name: Set the proper permissions for Nagios files
      file:
        path: "{{ item }}"
        owner: "{{ nagios_user }}"
        group: "{{ nagios_user }}"
        mode: "0755"
        recurse: yes
      with_items:
        - "/usr/local/nagios/etc"
        - "/usr/local/nagios/var"
        - "/usr/local/nagios/libexec"

    - name: Configure Nagios web interface password
      shell: htpasswd -b -c /usr/local/nagios/etc/htpasswd.users "{{ nagios_admin_user }}" "{{ nagios_admin_password }}"
      args:
        creates: "/usr/local/nagios/etc/htpasswd.users"

    - name: Start and enable Apache
      systemd:
        name: httpd
        state: started
        enabled: yes

    - name: Start and enable Nagios service
      systemd:
        name: nagios
        state: started
        enabled: yes

    - name: Allow HTTP service through firewall
      firewalld:
        service: http
        permanent: yes
        state: enabled

    - name: Reload firewall to apply changes
      firewalld:
        state: reloaded

    - name: Restart Apache to apply Nagios configuration
      systemd:
        name: httpd
        state: restarted
