import React, { useState } from 'react';

// Example usage
function App() {
  const A = [
    {
      provider: 'Provider 1',
      hospitals: [
        { id: 1, name: 'Hospital A', text: 'Some text A' },
        { id: 2, name: 'Hospital B', text: 'Some text B' },
      ],
    },
    {
      provider: 'Provider 2',
      hospitals: [
        { id: 2, name: 'Hospital C', text: 'Some text C' },
        { id: 4, name: 'Hospital D', text: 'Some text D' },
      ],
    },
    {
      provider: 'Provider 3',
      hospitals: [
        { id: 3, name: 'Hospital E', text: 'Some text E' },
        { id: 4, name: 'Hospital F', text: 'Some text F' },
      ],
    },
    {
      provider: 'Provider 4',
      hospitals: [
        { id: 3, name: 'Hospital G', text: 'Some text G' },
        { id: 4, name: 'Hospital H', text: 'Some text H' },
      ],
    },
  ];
  const temp = [{ id: 4 }, { id: 2 }, { id: 3}];

  const searchHospitalById = (itemId) => {
    let totalCount = 0;

    A.forEach((provider) => {
      provider.hospitals.forEach((hospital) => {
        if (hospital.id === itemId) {
          totalCount++;
        }
      });
    });

    return totalCount;
  };

  return (
    <div>
      <h1>Hospital Search</h1>
      {temp.map((item) => (
        <div key={item.id}>
          <p>ID: {item.id}
          {searchHospitalById(item.id)}
          </p>
        </div>
      ))}
    </div>
  );
}

export default App;
